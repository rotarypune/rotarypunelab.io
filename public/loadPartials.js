(function($, window, document, undefined) {
    
        $(function() {
            $("#header").load("./header.html");
            $("#commonNavbar").load("./navbar.html");
            $("#footer").load("./footer.html");
        });
    
    })(jQuery, window, document);